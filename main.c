#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

int CAD[5][5];
int CAD_AL_1[5][5]; //Sessao 1 - Alem da Vida
int CAD_AL_2[5][5]; //Sessao 2 - Alem da Vida
int CAD_MM_1[5][5]; //Sessao 1 - Os Melhores do Mundo
int CAD_MM_2[5][5]; //Sessao 2 - Os Melhores do Mundo
	int opcao,peca,t,m,g,sala;
	int tipo;
	int inteiro;
	int meia;
	int gratis;
	int just_beneficio;
	int menuadministrador;
	int i /*Fila Sessao 1*/;
	int y /*Fila Sessao 2*/;
	int j /*Assento Sessao 1*/;
	int k /*Assento Sessao 2*/;
	int dia;
	int mes;
	int ano;

int diadasemana(int d, int m, int y){ 
	static int t[] = { 0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4 }; 
	y -= m < 3; 
	return ( y + y/4 - y/100 + y/400 + t[m-1] + d) % 7; 
}

void Administrador(){
	system("cls");
	
	int valortotal_inteiro = inteiro * 20;
	int valortotal_meia = meia * 10;
	int totaldeingressos = inteiro + meia + gratis;
	int valortotalingressos = valortotal_inteiro + valortotal_meia;	
	
	printf("\t------FECHAMENTO DE CAIXA------\n\n");
	printf(" Informa��es gerais de vendas de ingressos: \n");
	printf("\n Total de ingressos (Inteira) vendidos: %d", inteiro);
	printf("\n Valor total de ingressos (Inteira) vendidos: R$ %d,00", valortotal_inteiro);
	printf("\n Total de ingressos (Meia) vendidos: %d", meia);
	printf("\n Valor total de ingressos (Meia) vendidos: R$ %d,00", valortotal_meia);
	printf("\n Total de ingressos (gratuitos) oferecidas: %d \n", gratis);
	printf("\n");
	printf("\n Total de ingressos gerados: %d ingressos\n", totaldeingressos);
	printf("\n Valor total de ingressos gerados: R$ %d,00 \n", valortotalingressos);
	printf("\t------------------------------\n");
	printf("\n");
	printf("\n Deseja voltar ao menu principal [1] ou, para sair do sistema, pressione qualquer outra tecla. ");
	scanf("%d", &menuadministrador);	
	switch(menuadministrador)
	{
	case 1:
		//Finaliza o switch atrav�s do case e volta ao menu principal.
		return;
	default:
		//Finaliza o sistema
		exit(Administrador);
	}
		
	  	getch();
		return 0;
}

void PrimeiraPeca(){ //PECA ALEM DA VIDA
	printf("\t------------------ALEM DA VIDA------------------\n");			
	printf("\tComprar bilhete para qual sessao ? \n\n");
	printf("\t Sessao [1]: 13:30 \n");
	printf("\t Sessao [2]: 18:30 \n");
	printf("\t------------------------------------------------\n\n");
	scanf("%d", &peca);
	switch(peca)
	{
		case 1:
			printf("\t------------------ALEM DA VIDA------------------\n");
			printf("\t----------------SESSAO DAS 13:30----------------\n\n");
			printf("\t \nFila: ");
			scanf("%d", &i);
			printf("\t \nAssento: ");
			scanf("%d", &j);
			printf("\t------------------------------------------------\n");
			printf("\t FORMAS DE PAGAMENTO : : : \n\n");
			printf("\t [1]Inteira: R$ 20,00 \n");
			printf("\t [2]Promocao de Terca-Feira \n");
			printf("\t [3]Meia: R$ 10,00 \n");
			
			scanf("%d", &tipo);
	
			switch(tipo)
			{				
				//--------- Pagamento selecionado como Inteiro -----------
				case 1:
					
					setlocale(LC_ALL, "Portuguese");
					
					if(CAD_AL_1[i][j]==0){
						
						printf("\t------------------------------------------------\n");
						printf("\tForma de Pagamento :: INTEIRA \n\n");	
						printf("\t ::: ALEM DA VIDA ::: \n");
						printf("\tSessao escolhida 1: 13:30\n\n");
						printf("\tPoltrona: Fileira: %d", i);
						printf("\tCadeira: %d \n\n", j);					
						printf("\tSeu bilhete foi adiquirido com sucesso. \n");
						
						printf("\tNos do Teatro UNIP - INTERATIVA agradecemos a preferencia. \n");
						
						printf("\tDesejamos que tenha uma otima peca. Divirta-se.\n ");
						
						printf("\t------------------------------------------------\n\n\n\n");
						//Adiciona o valor de R$ 20,00 dentro da vari�vel para a toda total no final.
						inteiro = inteiro + 1;
						break;
					
					} else {
					
						printf("\t*******************************\n");
						printf("\t****ATEN��O CADEIRA OCUPADA****\n");
						printf("\t***************OU**************\n");
						printf("\t**********INEXISTENTE**********\n");
						printf("\t*******************************\n");
						printf("\t******Processo reiniciado******\n");
						printf("\t*******************************\n\n");
						system ("pause");
						return 0;
					}
					
				//--------- Pagamento gratuito para crian�as carentes da rede p�blica -----------
				case 2:
					
					if(CAD_AL_1[i][j]==0){
						
						printf("\t \nInsira o dia(Formato: dd)");
						scanf("%d", &dia);
						printf("\t \nInsira o mes(Formato: mm)");
						scanf("%d", &mes);
						printf("\t \nInsira o ano(Formato: aaaa)");
						scanf("%d", &ano);
												
						int verificaterca = diadasemana(dia, mes, ano);
													
						if(verificaterca==2){
							printf("\t------------------------------------------------\n");
							printf("\tForma de Pagamento :: ENTRADA GRATUITA AS TER�AS \n\n");	
							printf("\t ::: ALEM DA VIDA ::: \n");
							printf("\tSessao escolhida 1: 13:30\n\n");
							printf("\tPoltrona: Fileira: %d", i);
							printf("\tCadeira: %d \n\n", j);					
							printf("\tSeu bilhete foi adiquirido com sucesso. \n");
							
							printf("\tNos do Teatro UNIP - INTERATIVA agradecemos a preferencia. \n");
							
							printf("\tDesejamos que tenha uma otima peca. Divirta-se.\n ");
							
							printf("\t------------------------------------------------\n\n\n\n");
							//Adiciona o valor de R$ 20,00 dentro da vari�vel para a toda total no final.
							gratis = gratis + 1;
							break;	
						}else{
							setlocale(LC_ALL, "Portuguese");
							printf("\t------------------------------------------------\n");
							printf("\tEsta data n�o � de uma Ter�a-Feira. \n");
							printf("\tTente novamente utilizando uma outra data. \n");
							printf("\t------------------------------------------------\n\n");
							system("pause");
							return 0;
						} 
				
					} else {
					
						printf("\t*******************************\n");
						printf("\t****ATENCAO CADEIRA OCUPADA****\n");
						printf("\t***************OU**************\n");
						printf("\t**********INEXISTENTE**********\n");
						printf("\t*******************************\n");
						printf("\t******Processo reiniciado******\n");
						printf("\t*******************************\n\n");
						system ("pause");
						return 0;
					}
				
				//--------- Pagamento gratuito para crian�as carentes da rede p�blica -----------	
				case 3:
					printf("\t------------------------------------------------\n\n\n\n");
					printf("\t  -JUSTIFICATIVA DA MEIA-ENTRADA- \n\n");
					printf("\t [1] Criancas de 02 a 12 anos de idade? \n\n");
					printf("\t [2] Adultos a partir de 60 anos? \n\n");
					printf("\t [3] Professor de rede publica de ensino? \n\n");
					scanf("%d", &just_beneficio);
					
					switch (just_beneficio)
					{
						//------- Justificativa de Meia entrada - Crian�as de 02 a 12 anos -------
						case 1:
							
							setlocale(LC_ALL, "Portuguese");
								
							if(CAD_AL_1[i][j]==0){
						
								printf("\t------------------------------------------------\n");
								printf("\tForma de Pagamento :: MEIA ENTRADA \n\n");	
								printf("\tCRIAN�AS DE 02 A 12 ANOS DE IDADE \n\n");	
								printf("\t ::: AL�M DA VIDA ::: \n");
								printf("\tSess�o escolhida 1: 13:30\n\n");
								printf("\tPoltrona: Fileira: %d", i);
								printf("\tCadeira: %d \n\n", j);					
								printf("\tSeu bilhete foi adiquirido com sucesso. \n");
								
								printf("\tN�s do Teatro UNIP - INTERATIVA agradecemos a prefer�ncia. \n");
								
								printf("\tDesejamos que tenha uma �tima pe�a. Divirta-se.\n ");
								
								printf("\t------------------------------------------------\n\n\n\n");
								
								//Gera um ticket de meia entrada no sistema dentro da vari�vel para ser contada na admin.
								meia = meia + 1;
								
								break;
												
							} else {
					
								printf("\t*******************************\n");
								printf("\t****ATEN��O CADEIRA OCUPADA****\n");
								printf("\t***************OU**************\n");
								printf("\t**********INEXISTENTE**********\n");
								printf("\t*******************************\n");
								printf("\t******Processo reiniciado******\n");
								printf("\t*******************************\n\n");
								system ("pause");
								return 0;
							}
							
						case 2:
							
							setlocale(LC_ALL, "Portuguese");
								
							if(CAD_AL_1[i][j]==0){
						
								printf("\t------------------------------------------------\n");
								printf("\tForma de Pagamento :: MEIA ENTRADA \n\n");	
								printf("\tADULTOS COM MAIS DE 60 ANOS DE IDADE \n\n");	
								printf("\t ::: AL�M DA VIDA ::: \n");
								printf("\tSess�o escolhida 1: 13:30\n\n");
								printf("\tPoltrona: Fileira: %d", i);
								printf("\tCadeira: %d \n\n", j);					
								printf("\tSeu bilhete foi adiquirido com sucesso. \n");
								
								printf("\tN�s do Teatro UNIP - INTERATIVA agradecemos a prefer�ncia. \n");
								
								printf("\tDesejamos que tenha uma �tima pe�a. Divirta-se.\n ");
								
								printf("\t------------------------------------------------\n\n\n\n");
								
								//Gera um ticket de meia entrada no sistema dentro da vari�vel para ser contada na admin.
								meia = meia + 1;
								
								break;
												
							} else {
					
								printf("\t*******************************\n");
								printf("\t****ATEN��O CADEIRA OCUPADA****\n");
								printf("\t***************OU**************\n");
								printf("\t**********INEXISTENTE**********\n");
								printf("\t*******************************\n");
								printf("\t******Processo reiniciado******\n");
								printf("\t*******************************\n\n");
								system ("pause");
								return 0;
							}			

						case 3:
						setlocale(LC_ALL, "Portuguese");
								
							if(CAD_AL_1[i][j]==0){
						
								printf("\t------------------------------------------------\n");
								printf("\tForma de Pagamento :: MEIA ENTRADA \n\n");	
								printf("\tBENEF�CIO PARA PROFESSORES DA REDE P�BLICA \n\n");	
								printf("\t ::: AL�M DA VIDA ::: \n");
								printf("\tSess�o escolhida 1: 13:30\n\n");
								printf("\tPoltrona: Fileira: %d", i);
								printf("\tCadeira: %d \n\n", j);					
								printf("\tSeu bilhete foi adiquirido com sucesso. \n");
								
								printf("\tN�s do Teatro UNIP - INTERATIVA agradecemos a prefer�ncia. \n");
								
								printf("\tDesejamos que tenha uma �tima pe�a. Divirta-se.\n ");
								
								printf("\t------------------------------------------------\n\n\n\n");
								
								//Gera um ticket de meia entrada no sistema dentro da vari�vel para ser contada na admin.
								meia = meia + 1;
								
								break;
												
							} else {
					
								printf("\t*******************************\n");
								printf("\t****ATEN��O CADEIRA OCUPADA****\n");
								printf("\t***************OU**************\n");
								printf("\t**********INEXISTENTE**********\n");
								printf("\t*******************************\n");
								printf("\t******Processo reiniciado******\n");
								printf("\t*******************************\n\n");
								system ("pause");
								return 0;
							}		
									
					} //Finaliza switch de benefici�rios do case 3.
					
					break;
					
			} //Finaliza o case TIPO 1
			
			//Realiza a �ltima op��o de cadeira ocupada.
			
			if(CAD_AL_1[i][j]==0){
			
				CAD_AL_1[i][j] = 1;	
			
			} else {
			
				printf("\t*******************************\n");
				printf("\t****ATENCAO CADEIRA OCUPADA****\n");
				printf("\t***************OU**************\n");
				printf("\t**********INEXISTENTE**********\n");
				printf("\t*******************************\n");
				printf("\t******Processo reiniciado******\n");
				printf("\t*******************************\n\n");
				system ("pause");
			
				return PrimeiraPeca;
		
			}
	
			{ 
			
				system ("pause");
			}
	
			break;
	
			// Inicia o case da pe�a 1 com a sess�o 2 (18h30)
			case 2:
				printf("\t------------------ALEM DA VIDA------------------\n");
				printf("\t----------------SESSAO DAS 18:30----------------\n\n");
				printf("\t \nFila: ");
				scanf("%d", &y);
				printf("\t \nAssento: ");
				scanf("%d", &k);
				printf("\t------------------------------------------------\n");
				
				printf("\t FORMAS DE PAGAMENTO : : : \n\n");
				printf("\t [1]Inteira: R$ 20,00 \n");
				printf("\t [2]Promocao de Terca-Feira \n");
				printf("\t [3]Meia: R$ 10,00 \n");
				scanf("%d", &tipo);
				
				switch(tipo){
					//--------- Pagamento selecionado como Inteiro -----------
				case 1:
					
					setlocale(LC_ALL, "Portuguese");
					
					if(CAD_AL_2[i][j]==0){
						
						printf("\t------------------------------------------------\n");
						printf("\tForma de Pagamento :: INTEIRA \n\n");	
						printf("\t ::: ALEM DA VIDA ::: \n");
						printf("\tSessao escolhida 1: 13:30\n\n");
						printf("\tPoltrona: Fileira: %d", i);
						printf("\tCadeira: %d \n\n", j);					
						printf("\tSeu bilhete foi adiquirido com sucesso. \n");
						
						printf("\tNos do Teatro UNIP - INTERATIVA agradecemos a preferencia. \n");
						
						printf("\tDesejamos que tenha uma otima peca. Divirta-se.\n ");
						
						printf("\t------------------------------------------------\n\n\n\n");
						//Adiciona o valor de R$ 20,00 dentro da vari�vel para a toda total no final.
						inteiro = inteiro + 1;
						break;
					
					} else {
					
						printf("\t*******************************\n");
						printf("\t****ATEN��O CADEIRA OCUPADA****\n");
						printf("\t***************OU**************\n");
						printf("\t**********INEXISTENTE**********\n");
						printf("\t*******************************\n");
						printf("\t******Processo reiniciado******\n");
						printf("\t*******************************\n\n");
						system ("pause");
						return 0;
					}
					
				//--------- Pagamento gratuito para crian�as carentes da rede p�blica -----------
				case 2:
					
					if(CAD_AL_2[i][j]==0){
						
						printf("\t \nInsira o dia(Formato: dd)");
						scanf("%d", &dia);
						printf("\t \nInsira o mes(Formato: mm)");
						scanf("%d", &mes);
						printf("\t \nInsira o ano(Formato: aaaa)");
						scanf("%d", &ano);
												
						int verificaterca = diadasemana(dia, mes, ano);
													
						if(verificaterca==2){
							printf("\t------------------------------------------------\n");
							printf("\tForma de Pagamento :: ENTRADA GRATUITA AS TER�AS \n\n");	
							printf("\t ::: ALEM DA VIDA ::: \n");
							printf("\tSessao escolhida 1: 13:30\n\n");
							printf("\tPoltrona: Fileira: %d", i);
							printf("\tCadeira: %d \n\n", j);					
							printf("\tSeu bilhete foi adiquirido com sucesso. \n");
							
							printf("\tNos do Teatro UNIP - INTERATIVA agradecemos a preferencia. \n");
							
							printf("\tDesejamos que tenha uma otima peca. Divirta-se.\n ");
							
							printf("\t------------------------------------------------\n\n\n\n");
							//Adiciona o valor de R$ 20,00 dentro da vari�vel para a toda total no final.
							gratis = gratis + 1;
							break;	
						}else{
							setlocale(LC_ALL, "Portuguese");
							printf("\t------------------------------------------------\n");
							printf("\tEsta data n�o � de uma Ter�a-Feira. \n");
							printf("\tTente novamente utilizando uma outra data. \n");
							printf("\t------------------------------------------------\n\n");
							system("pause");
							return 0;
						} 
				
					} else {
					
						printf("\t*******************************\n");
						printf("\t****ATENCAO CADEIRA OCUPADA****\n");
						printf("\t***************OU**************\n");
						printf("\t**********INEXISTENTE**********\n");
						printf("\t*******************************\n");
						printf("\t******Processo reiniciado******\n");
						printf("\t*******************************\n\n");
						system ("pause");
						return 0;
					}
				
				//--------- Pagamento gratuito para crian�as carentes da rede p�blica -----------	
				case 3:
					printf("\t------------------------------------------------\n\n\n\n");
					printf("\t  -JUSTIFICATIVA DA MEIA-ENTRADA- \n\n");
					printf("\t [1] Criancas de 02 a 12 anos de idade? \n\n");
					printf("\t [2] Adultos a partir de 60 anos? \n\n");
					printf("\t [3] Professor de rede publica de ensino? \n\n");
					scanf("%d", &just_beneficio);
					
					switch (just_beneficio)
					{
						//------- Justificativa de Meia entrada - Crian�as de 02 a 12 anos -------
						case 1:
							
							setlocale(LC_ALL, "Portuguese");
								
							if(CAD_AL_2[i][j]==0){
						
								printf("\t------------------------------------------------\n");
								printf("\tForma de Pagamento :: MEIA ENTRADA \n\n");	
								printf("\tCRIAN�AS DE 02 A 12 ANOS DE IDADE \n\n");	
								printf("\t ::: AL�M DA VIDA ::: \n");
								printf("\tSess�o escolhida 1: 13:30\n\n");
								printf("\tPoltrona: Fileira: %d", i);
								printf("\tCadeira: %d \n\n", j);					
								printf("\tSeu bilhete foi adiquirido com sucesso. \n");
								
								printf("\tN�s do Teatro UNIP - INTERATIVA agradecemos a prefer�ncia. \n");
								
								printf("\tDesejamos que tenha uma �tima pe�a. Divirta-se.\n ");
								
								printf("\t------------------------------------------------\n\n\n\n");
								
								//Gera um ticket de meia entrada no sistema dentro da vari�vel para ser contada na admin.
								meia = meia + 1;
								
								break;
												
							} else {
					
								printf("\t*******************************\n");
								printf("\t****ATEN��O CADEIRA OCUPADA****\n");
								printf("\t***************OU**************\n");
								printf("\t**********INEXISTENTE**********\n");
								printf("\t*******************************\n");
								printf("\t******Processo reiniciado******\n");
								printf("\t*******************************\n\n");
								system ("pause");
								return 0;
							}
							
						case 2:
							
							setlocale(LC_ALL, "Portuguese");
								
							if(CAD_AL_2[i][j]==0){
						
								printf("\t------------------------------------------------\n");
								printf("\tForma de Pagamento :: MEIA ENTRADA \n\n");	
								printf("\tADULTOS COM MAIS DE 60 ANOS DE IDADE \n\n");	
								printf("\t ::: AL�M DA VIDA ::: \n");
								printf("\tSess�o escolhida 1: 13:30\n\n");
								printf("\tPoltrona: Fileira: %d", i);
								printf("\tCadeira: %d \n\n", j);					
								printf("\tSeu bilhete foi adiquirido com sucesso. \n");
								
								printf("\tN�s do Teatro UNIP - INTERATIVA agradecemos a prefer�ncia. \n");
								
								printf("\tDesejamos que tenha uma �tima pe�a. Divirta-se.\n ");
								
								printf("\t------------------------------------------------\n\n\n\n");
								
								//Gera um ticket de meia entrada no sistema dentro da vari�vel para ser contada na admin.
								meia = meia + 1;
								
								break;
												
							} else {
					
								printf("\t*******************************\n");
								printf("\t****ATEN��O CADEIRA OCUPADA****\n");
								printf("\t***************OU**************\n");
								printf("\t**********INEXISTENTE**********\n");
								printf("\t*******************************\n");
								printf("\t******Processo reiniciado******\n");
								printf("\t*******************************\n\n");
								system ("pause");
								return 0;
							}			

						case 3:
						setlocale(LC_ALL, "Portuguese");
								
							if(CAD_AL_2[i][j]==0){
						
								printf("\t------------------------------------------------\n");
								printf("\tForma de Pagamento :: MEIA ENTRADA \n\n");	
								printf("\tBENEF�CIO PARA PROFESSORES DA REDE P�BLICA \n\n");	
								printf("\t ::: AL�M DA VIDA ::: \n");
								printf("\tSess�o escolhida 1: 13:30\n\n");
								printf("\tPoltrona: Fileira: %d", i);
								printf("\tCadeira: %d \n\n", j);					
								printf("\tSeu bilhete foi adiquirido com sucesso. \n");
								
								printf("\tN�s do Teatro UNIP - INTERATIVA agradecemos a prefer�ncia. \n");
								
								printf("\tDesejamos que tenha uma �tima pe�a. Divirta-se.\n ");
								
								printf("\t------------------------------------------------\n\n\n\n");
								
								//Gera um ticket de meia entrada no sistema dentro da vari�vel para ser contada na admin.
								meia = meia + 1;
								
								break;
												
							} else {
					
								printf("\t*******************************\n");
								printf("\t****ATEN��O CADEIRA OCUPADA****\n");
								printf("\t***************OU**************\n");
								printf("\t**********INEXISTENTE**********\n");
								printf("\t*******************************\n");
								printf("\t******Processo reiniciado******\n");
								printf("\t*******************************\n\n");
								system ("pause");
								return 0;
							}		
									
					} //Finaliza switch de benefici�rios do case 3.
					
					break;
					
			} 
			
			if(CAD_AL_2[y][k]==0){
				
				CAD_AL_2[y][k] = 1;
	
			}else{
				
				printf("\t*******************************\n");
				printf("\t****ATENCAO CADEIRA OCUPADA****\n");
				printf("\t***************OU**************\n");
				printf("\t**********INEXISTENTE**********\n");
				printf("\t*******************************\n");
				printf("\t******Processo reiniciado******\n");
				printf("\t*******************************\n\n");
				return PrimeiraPeca;
			}
			
			{ system ("pause"); }	
		
			break;
		}
	}

void SegundaPeca(){ //PECA OS MELHORES DO MUNDO
	printf("\t------------------OS MELHORES DO MUNDO------------------\n");			
	printf("\tComprar bilhete para qual sessao ? \n\n");
	printf("\t Sessao [1]: 13:30 \n");
	printf("\t Sessao [2]: 18:30 \n");
	printf("\t--------------------------------------------------------\n\n");
	scanf("%d", &peca);
	switch(peca)
	{
		case 1:
			printf("\t------------------OS MELHORES DO MUNDO------------------\n");
			printf("\t--------------------SESSAO DAS 13:30--------------------\n\n");
			printf("\t \nFila: ");
			scanf("%d", &i);
			printf("\t \nAssento: ");
			scanf("%d", &j);
			printf("\t--------------------------------------------------------\n");
			printf("\t FORMAS DE PAGAMENTO : : : \n\n");
			printf("\t [1]Inteira: R$ 20,00 \n");
			printf("\t [2]Promocao de Terca-Feira \n");
			printf("\t [3]Meia: R$ 10,00 \n");
			
			scanf("%d", &tipo);
	
			switch(tipo)
			{				
				//--------- Pagamento selecionado como Inteiro -----------
				case 1:
					
					setlocale(LC_ALL, "Portuguese");
					
					if(CAD_MM_1[i][j]==0){
						
						printf("\t------------------------------------------------\n");
						printf("\tForma de Pagamento :: INTEIRA \n\n");	
						printf("\t ::: OS MELHORES DO MUNDO ::: \n");
						printf("\tSessao escolhida 1: 13:30\n\n");
						printf("\tPoltrona: Fileira: %d", i);
						printf("\tCadeira: %d \n\n", j);					
						printf("\tSeu bilhete foi adiquirido com sucesso. \n");
						
						printf("\tNos do Teatro UNIP - INTERATIVA agradecemos a preferencia. \n");
						
						printf("\tDesejamos que tenha uma otima peca. Divirta-se.\n ");
						
						printf("\t------------------------------------------------\n\n\n\n");
						//Adiciona o valor de R$ 20,00 dentro da vari�vel para a toda total no final.
						inteiro = inteiro + 1;
						break;
					
					} else {
					
						printf("\t*******************************\n");
						printf("\t****ATEN��O CADEIRA OCUPADA****\n");
						printf("\t***************OU**************\n");
						printf("\t**********INEXISTENTE**********\n");
						printf("\t*******************************\n");
						printf("\t******Processo reiniciado******\n");
						printf("\t*******************************\n\n");
						system ("pause");
						return 0;
					}
					
				//--------- Pagamento gratuito para crian�as carentes da rede p�blica -----------
				case 2:
					
					if(CAD_MM_1[i][j]==0){
						
						printf("\t \nInsira o dia(Formato: dd)");
						scanf("%d", &dia);
						printf("\t \nInsira o mes(Formato: mm)");
						scanf("%d", &mes);
						printf("\t \nInsira o ano(Formato: aaaa)");
						scanf("%d", &ano);
												
						int verificaterca = diadasemana(dia, mes, ano);
													
						if(verificaterca==2){
							printf("\t------------------------------------------------\n");
							printf("\tForma de Pagamento :: ENTRADA GRATUITA AS TER�AS \n\n");	
							printf("\t ::: OS MELHORES DO MUNDO ::: \n");
							printf("\tSessao escolhida 1: 13:30\n\n");
							printf("\tPoltrona: Fileira: %d", i);
							printf("\tCadeira: %d \n\n", j);					
							printf("\tSeu bilhete foi adiquirido com sucesso. \n");
							
							printf("\tNos do Teatro UNIP - INTERATIVA agradecemos a preferencia. \n");
							
							printf("\tDesejamos que tenha uma otima peca. Divirta-se.\n ");
							
							printf("\t------------------------------------------------\n\n\n\n");
							//Adiciona o valor de R$ 20,00 dentro da vari�vel para a toda total no final.
							gratis = gratis + 1;
							break;	
						}else{
							setlocale(LC_ALL, "Portuguese");
							printf("\t------------------------------------------------\n");
							printf("\tEsta data n�o � de uma Ter�a-Feira. \n");
							printf("\tTente novamente utilizando uma outra data. \n");
							printf("\t------------------------------------------------\n\n");
							system("pause");
							return 0;
						} 
				
					} else {
					
						printf("\t*******************************\n");
						printf("\t****ATENCAO CADEIRA OCUPADA****\n");
						printf("\t***************OU**************\n");
						printf("\t**********INEXISTENTE**********\n");
						printf("\t*******************************\n");
						printf("\t******Processo reiniciado******\n");
						printf("\t*******************************\n\n");
						system ("pause");
						return 0;
					}
				
				//--------- Pagamento gratuito para crian�as carentes da rede p�blica -----------	
				case 3:
					printf("\t------------------------------------------------\n\n\n\n");
					printf("\t  -JUSTIFICATIVA DA MEIA-ENTRADA- \n\n");
					printf("\t [1] Criancas de 02 a 12 anos de idade? \n\n");
					printf("\t [2] Adultos a partir de 60 anos? \n\n");
					printf("\t [3] Professor de rede publica de ensino? \n\n");
					scanf("%d", &just_beneficio);
					
					switch (just_beneficio)
					{
						//------- Justificativa de Meia entrada - Crian�as de 02 a 12 anos -------
						case 1:
							
							setlocale(LC_ALL, "Portuguese");
								
							if(CAD_MM_1[i][j]==0){
						
								printf("\t------------------------------------------------\n");
								printf("\tForma de Pagamento :: MEIA ENTRADA \n\n");	
								printf("\tCRIAN�AS DE 02 A 12 ANOS DE IDADE \n\n");	
								printf("\t ::: OS MELHORES DO MUNDO ::: \n");
								printf("\tSess�o escolhida 1: 13:30\n\n");
								printf("\tPoltrona: Fileira: %d", i);
								printf("\tCadeira: %d \n\n", j);					
								printf("\tSeu bilhete foi adiquirido com sucesso. \n");
								
								printf("\tN�s do Teatro UNIP - INTERATIVA agradecemos a prefer�ncia. \n");
								
								printf("\tDesejamos que tenha uma �tima pe�a. Divirta-se.\n ");
								
								printf("\t------------------------------------------------\n\n\n\n");
								
								//Gera um ticket de meia entrada no sistema dentro da vari�vel para ser contada na admin.
								meia = meia + 1;
								
								break;
												
							} else {
					
								printf("\t*******************************\n");
								printf("\t****ATEN��O CADEIRA OCUPADA****\n");
								printf("\t***************OU**************\n");
								printf("\t**********INEXISTENTE**********\n");
								printf("\t*******************************\n");
								printf("\t******Processo reiniciado******\n");
								printf("\t*******************************\n\n");
								system ("pause");
								return 0;
							}
							
						case 2:
							
							setlocale(LC_ALL, "Portuguese");
								
							if(CAD_MM_1[i][j]==0){
						
								printf("\t------------------------------------------------\n");
								printf("\tForma de Pagamento :: MEIA ENTRADA \n\n");	
								printf("\tADULTOS COM MAIS DE 60 ANOS DE IDADE \n\n");	
								printf("\t ::: OS MELHORES DO MUNDO ::: \n");
								printf("\tSess�o escolhida 1: 13:30\n\n");
								printf("\tPoltrona: Fileira: %d", i);
								printf("\tCadeira: %d \n\n", j);					
								printf("\tSeu bilhete foi adiquirido com sucesso. \n");
								
								printf("\tN�s do Teatro UNIP - INTERATIVA agradecemos a prefer�ncia. \n");
								
								printf("\tDesejamos que tenha uma �tima pe�a. Divirta-se.\n ");
								
								printf("\t------------------------------------------------\n\n\n\n");
								
								//Gera um ticket de meia entrada no sistema dentro da vari�vel para ser contada na admin.
								meia = meia + 1;
								
								break;
												
							} else {
					
								printf("\t*******************************\n");
								printf("\t****ATEN��O CADEIRA OCUPADA****\n");
								printf("\t***************OU**************\n");
								printf("\t**********INEXISTENTE**********\n");
								printf("\t*******************************\n");
								printf("\t******Processo reiniciado******\n");
								printf("\t*******************************\n\n");
								system ("pause");
								return 0;
							}			

						case 3:
						setlocale(LC_ALL, "Portuguese");
								
							if(CAD_MM_1[i][j]==0){
						
								printf("\t------------------------------------------------\n");
								printf("\tForma de Pagamento :: MEIA ENTRADA \n\n");	
								printf("\tBENEF�CIO PARA PROFESSORES DA REDE P�BLICA \n\n");	
								printf("\t ::: OS MELHORES DO MUNDO ::: \n");
								printf("\tSess�o escolhida 1: 13:30\n\n");
								printf("\tPoltrona: Fileira: %d", i);
								printf("\tCadeira: %d \n\n", j);					
								printf("\tSeu bilhete foi adiquirido com sucesso. \n");
								
								printf("\tN�s do Teatro UNIP - INTERATIVA agradecemos a prefer�ncia. \n");
								
								printf("\tDesejamos que tenha uma �tima pe�a. Divirta-se.\n ");
								
								printf("\t------------------------------------------------\n\n\n\n");
								
								//Gera um ticket de meia entrada no sistema dentro da vari�vel para ser contada na admin.
								meia = meia + 1;
								
								break;
												
							} else {
					
								printf("\t*******************************\n");
								printf("\t****ATEN��O CADEIRA OCUPADA****\n");
								printf("\t***************OU**************\n");
								printf("\t**********INEXISTENTE**********\n");
								printf("\t*******************************\n");
								printf("\t******Processo reiniciado******\n");
								printf("\t*******************************\n\n");
								system ("pause");
								return 0;
							}		
									
					} //Finaliza switch de benefici�rios do case 3.
					
					break;
					
			} //Finaliza o case TIPO 1
			
			//Realiza a �ltima op��o de cadeira ocupada.
			
			if(CAD_MM_1[i][j]==0){
			
				CAD_MM_1[i][j] = 1;	
			
			} else {
			
				printf("\t*******************************\n");
				printf("\t****ATENCAO CADEIRA OCUPADA****\n");
				printf("\t***************OU**************\n");
				printf("\t**********INEXISTENTE**********\n");
				printf("\t*******************************\n");
				printf("\t******Processo reiniciado******\n");
				printf("\t*******************************\n\n");
				system ("pause");
			
				return PrimeiraPeca;
		
			}
	
			{ 
			
				system ("pause");
			}
	
			break;
	
			// Inicia o case da pe�a 1 com a sess�o 2 (18h30)
			case 2:
				printf("\t------------------OS MELHORES DO MUNDO------------------\n");
				printf("\t--------------------SESSAO DAS 18:30--------------------\n\n");
				printf("\t \nFila: ");
				scanf("%d", &y);
				printf("\t \nAssento: ");
				scanf("%d", &k);
				printf("\t--------------------------------------------------------\n");
				
				printf("\t FORMAS DE PAGAMENTO : : : \n\n");
				printf("\t [1]Inteira: R$ 20,00 \n");
				printf("\t [2]Promocao de Terca-Feira \n");
				printf("\t [3]Meia: R$ 10,00 \n");
				scanf("%d", &tipo);
				
				switch(tipo){
					//--------- Pagamento selecionado como Inteiro -----------
				case 1:
					
					setlocale(LC_ALL, "Portuguese");
					
					if(CAD_MM_2[i][j]==0){
						
						printf("\t------------------------------------------------\n");
						printf("\tForma de Pagamento :: INTEIRA \n\n");	
						printf("\t ::: OS MELHORES DO MUNDO ::: \n");
						printf("\tSessao escolhida 1: 13:30\n\n");
						printf("\tPoltrona: Fileira: %d", i);
						printf("\tCadeira: %d \n\n", j);					
						printf("\tSeu bilhete foi adiquirido com sucesso. \n");
						
						printf("\tNos do Teatro UNIP - INTERATIVA agradecemos a preferencia. \n");
						
						printf("\tDesejamos que tenha uma otima peca. Divirta-se.\n ");
						
						printf("\t------------------------------------------------\n\n\n\n");
						//Adiciona o valor de R$ 20,00 dentro da vari�vel para a toda total no final.
						inteiro = inteiro + 1;
						break;
					
					} else {
					
						printf("\t*******************************\n");
						printf("\t****ATEN��O CADEIRA OCUPADA****\n");
						printf("\t***************OU**************\n");
						printf("\t**********INEXISTENTE**********\n");
						printf("\t*******************************\n");
						printf("\t******Processo reiniciado******\n");
						printf("\t*******************************\n\n");
						system ("pause");
						return 0;
					}
					
				//--------- Pagamento gratuito para crian�as carentes da rede p�blica -----------
				case 2:
					
					if(CAD_MM_2[i][j]==0){
						
						printf("\t \nInsira o dia(Formato: dd)");
						scanf("%d", &dia);
						printf("\t \nInsira o mes(Formato: mm)");
						scanf("%d", &mes);
						printf("\t \nInsira o ano(Formato: aaaa)");
						scanf("%d", &ano);
												
						int verificaterca = diadasemana(dia, mes, ano);
													
						if(verificaterca==2){
							printf("\t------------------------------------------------\n");
							printf("\tForma de Pagamento :: ENTRADA GRATUITA AS TER�AS \n\n");	
							printf("\t ::: OS MELHORES DO MUNDO ::: \n");
							printf("\tSessao escolhida 1: 13:30\n\n");
							printf("\tPoltrona: Fileira: %d", i);
							printf("\tCadeira: %d \n\n", j);					
							printf("\tSeu bilhete foi adiquirido com sucesso. \n");
							
							printf("\tNos do Teatro UNIP - INTERATIVA agradecemos a preferencia. \n");
							
							printf("\tDesejamos que tenha uma otima peca. Divirta-se.\n ");
							
							printf("\t------------------------------------------------\n\n\n\n");
							//Adiciona o valor de R$ 20,00 dentro da vari�vel para a toda total no final.
							gratis = gratis + 1;
							break;	
						}else{
							setlocale(LC_ALL, "Portuguese");
							printf("\t------------------------------------------------\n");
							printf("\tEsta data n�o � de uma Ter�a-Feira. \n");
							printf("\tTente novamente utilizando uma outra data. \n");
							printf("\t------------------------------------------------\n\n");
							system("pause");
							return 0;
						} 
				
					} else {
					
						printf("\t*******************************\n");
						printf("\t****ATENCAO CADEIRA OCUPADA****\n");
						printf("\t***************OU**************\n");
						printf("\t**********INEXISTENTE**********\n");
						printf("\t*******************************\n");
						printf("\t******Processo reiniciado******\n");
						printf("\t*******************************\n\n");
						system ("pause");
						return 0;
					}
				
				//--------- Pagamento gratuito para crian�as carentes da rede p�blica -----------	
				case 3:
					printf("\t------------------------------------------------\n\n\n\n");
					printf("\t  -JUSTIFICATIVA DA MEIA-ENTRADA- \n\n");
					printf("\t [1] Criancas de 02 a 12 anos de idade? \n\n");
					printf("\t [2] Adultos a partir de 60 anos? \n\n");
					printf("\t [3] Professor de rede publica de ensino? \n\n");
					scanf("%d", &just_beneficio);
					
					switch (just_beneficio)
					{
						//------- Justificativa de Meia entrada - Crian�as de 02 a 12 anos -------
						case 1:
							
							setlocale(LC_ALL, "Portuguese");
								
							if(CAD_MM_2[i][j]==0){
						
								printf("\t------------------------------------------------\n");
								printf("\tForma de Pagamento :: MEIA ENTRADA \n\n");	
								printf("\tCRIAN�AS DE 02 A 12 ANOS DE IDADE \n\n");	
								printf("\t ::: OS MELHORES DO MUNDO ::: \n");
								printf("\tSess�o escolhida 1: 13:30\n\n");
								printf("\tPoltrona: Fileira: %d", i);
								printf("\tCadeira: %d \n\n", j);					
								printf("\tSeu bilhete foi adiquirido com sucesso. \n");
								
								printf("\tN�s do Teatro UNIP - INTERATIVA agradecemos a prefer�ncia. \n");
								
								printf("\tDesejamos que tenha uma �tima pe�a. Divirta-se.\n ");
								
								printf("\t------------------------------------------------\n\n\n\n");
								
								//Gera um ticket de meia entrada no sistema dentro da vari�vel para ser contada na admin.
								meia = meia + 1;
								
								break;
												
							} else {
					
								printf("\t*******************************\n");
								printf("\t****ATEN��O CADEIRA OCUPADA****\n");
								printf("\t***************OU**************\n");
								printf("\t**********INEXISTENTE**********\n");
								printf("\t*******************************\n");
								printf("\t******Processo reiniciado******\n");
								printf("\t*******************************\n\n");
								system ("pause");
								return 0;
							}
							
						case 2:
							
							setlocale(LC_ALL, "Portuguese");
								
							if(CAD_MM_2[i][j]==0){
						
								printf("\t------------------------------------------------\n");
								printf("\tForma de Pagamento :: MEIA ENTRADA \n\n");	
								printf("\tADULTOS COM MAIS DE 60 ANOS DE IDADE \n\n");	
								printf("\t ::: OS MELHORES DO MUNDO ::: \n");
								printf("\tSess�o escolhida 1: 13:30\n\n");
								printf("\tPoltrona: Fileira: %d", i);
								printf("\tCadeira: %d \n\n", j);					
								printf("\tSeu bilhete foi adiquirido com sucesso. \n");
								
								printf("\tN�s do Teatro UNIP - INTERATIVA agradecemos a prefer�ncia. \n");
								
								printf("\tDesejamos que tenha uma �tima pe�a. Divirta-se.\n ");
								
								printf("\t------------------------------------------------\n\n\n\n");
								
								//Gera um ticket de meia entrada no sistema dentro da vari�vel para ser contada na admin.
								meia = meia + 1;
								
								break;
												
							} else {
					
								printf("\t*******************************\n");
								printf("\t****ATEN��O CADEIRA OCUPADA****\n");
								printf("\t***************OU**************\n");
								printf("\t**********INEXISTENTE**********\n");
								printf("\t*******************************\n");
								printf("\t******Processo reiniciado******\n");
								printf("\t*******************************\n\n");
								system ("pause");
								return 0;
							}			

						case 3:
						setlocale(LC_ALL, "Portuguese");
								
							if(CAD_MM_2[i][j]==0){
						
								printf("\t------------------------------------------------\n");
								printf("\tForma de Pagamento :: MEIA ENTRADA \n\n");	
								printf("\tBENEF�CIO PARA PROFESSORES DA REDE P�BLICA \n\n");	
								printf("\t ::: OS MELHORES DO MUNDO ::: \n");
								printf("\tSess�o escolhida 1: 13:30\n\n");
								printf("\tPoltrona: Fileira: %d", i);
								printf("\tCadeira: %d \n\n", j);					
								printf("\tSeu bilhete foi adiquirido com sucesso. \n");
								
								printf("\tN�s do Teatro UNIP - INTERATIVA agradecemos a prefer�ncia. \n");
								
								printf("\tDesejamos que tenha uma �tima pe�a. Divirta-se.\n ");
								
								printf("\t------------------------------------------------\n\n\n\n");
								
								//Gera um ticket de meia entrada no sistema dentro da vari�vel para ser contada na admin.
								meia = meia + 1;
								
								break;
												
							} else {
					
								printf("\t*******************************\n");
								printf("\t****ATEN��O CADEIRA OCUPADA****\n");
								printf("\t***************OU**************\n");
								printf("\t**********INEXISTENTE**********\n");
								printf("\t*******************************\n");
								printf("\t******Processo reiniciado******\n");
								printf("\t*******************************\n\n");
								system ("pause");
								return 0;
							}		
									
					} //Finaliza switch de benefici�rios do case 3.
					
					break;
					
			} 
			
			if(CAD_MM_2[y][k]==0){
				
				CAD_MM_2[y][k] = 1;
	
			}else{
				
				printf("\t*******************************\n");
				printf("\t****ATENCAO CADEIRA OCUPADA****\n");
				printf("\t***************OU**************\n");
				printf("\t**********INEXISTENTE**********\n");
				printf("\t*******************************\n");
				printf("\t******Processo reiniciado******\n");
				printf("\t*******************************\n\n");
				return PrimeiraPeca;
			}
			
			{ system ("pause"); }	
		
			break;
		}

}

void Cancelar(){
			system("cls");
			printf("\t------------------------------------------------\n");
			printf("\t Opcao sair selecionada. \n\n");
			printf("\t Ficamos a disposicao. \n");
			printf("\t------------------------------------------------\n");
			
			//Fun��o para finalizar a execu��o do sistema
			exit(Cancelar);
			return 0;
}

int main(void){
	
	for(i = 0; i < 5; i++){
		for(j = 0; j < 5; j++){
			CAD[i][j]=0;
		}
	}
	
	do{
		
		system("cls");
	
		printf("\t================================================\n");
		
		printf("\t=================Seja Bem-Vindo=================\n");
		
		printf("\t======================ao========================\n");
		
		printf("\t=============TEATRO UNIP INTERATIVA============\n");
		
		printf("\t================================================\n\n");
		
		printf("\t ESCOLHA ABAIXO SUA OPCAO: \n");
		
		printf("\t------------------------------------------------\n");
		
		printf("\t| [1] Venda de ingressos - Alem da Vida |\n");
		
		printf("\t| [2] Venda de ingressos - Os Melhores do Mundo |\n");
		
		printf("\t| [99] Fechamento de caixa\n ");
		
		printf("\t| [0] Sair do sistema \n");		
		
		scanf("%d", &opcao);
		
		printf("\t------------------------------------------------\n");
		
		
		
		switch(opcao){
			case 1:
				PrimeiraPeca();
				break;
			case 2:
				SegundaPeca();
				break;
			case 99:
				Administrador();
				break;
			case 0:
				Cancelar();
				break;
		}
	}while(opcao!=5);
	
	system("pause");
	return 0;
}
